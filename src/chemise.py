from __future__ import print_function
from __future__ import division

import os
import re
import itertools

from numpy import *
import scipy.constants as co
from scipy import sparse
from scipy.interpolate import UnivariateSpline, interp1d
import _chemise

class Reaction(object):
    def __init__(self, lhs, rhs, rate, attrs={}):
        self.lhs = lhs
        self.rhs = rhs
        self.rate = rate
        self.attrs = attrs
        
    def rate(self, status):
        raise NotImplementedError("You must specify the reaction rate")
    

    def latex(self):
        return r"\cee{%s}" % self.tostr(times='', align='&')
    

    def tostr(self, times='*', align=''):
        def multi(n, s):
            if n == 1:
                return s
            else:
                return "%d %s %s" % (n, times, s)
        
        def joinside(side):
            return " + ".join(multi(n, s) for n, s in side)

        signature = "%s %s-> %s" % (joinside(self.lhs), align,
                                    joinside(self.rhs))

        return signature

    
    def __str__(self):
        return self.tostr()
    

class ReactionSet(object):
    def __init__(self):
        self.dspecies = dict()
        self.species = []
        self.reactions = []
        self.nspecies = 0
        self.nreactions = 0
        self.fixed = dict()
        self.composite = dict()

        
    def fix(self, d):
        self.fixed.update(d)

        
    def compose(self, d):
        self.composite.update(d)


    def isvoid(self, s):
        """ Checks whether the species is 'void' (i.e. fixed or composite.
        """
        return s in self.fixed or s in self.composite

    
    def add(self, signature, rate, **kwargs):
        lhs, rhs = parse(signature)
        for n, s in lhs + rhs:
            if not s in self.dspecies and not self.isvoid(s):
                self.dspecies[s] = self.nspecies
                self.species.append(s)
                self.nspecies += 1
                
        r = Reaction(lhs, rhs, rate, attrs=kwargs)
        self.reactions.append(r)
        self.nreactions += 1

        return r

    
    def add2(self, signature, rate_for, rate_back, **kwargs):
        r = self.add(signature, rate_for, **kwargs)
        self.reactions.append(Reaction(r.rhs, r.lhs, rate_back, attrs=kwargs))
        self.nreactions += 1


    def add_pattern(self, signature_pattern, substs, rate, **kwargs):
        generators = [[(k, item) for item in itemlist]
                      for k, itemlist in substs.items()]
        for replacements in itertools.product(*generators):
            signature = signature_pattern.format(**dict(replacements))
            self.add(signature, rate, **kwargs)


    def initialize_composites(self):
        """ Deals with composite species creating a new dictionary with indexes
        for them.   We also create the matrix C that composes species.  
        """
        self.c_species = [s for s in self.species]
        self.c_nspecies = self.nspecies
        self.c_dspecies = self.dspecies.copy()
        
        if not self.composite:
            self.C = None
            return
        
        for s in self.composite:
            self.c_dspecies[s] = self.c_nspecies
            self.c_species.append(s)
            self.c_nspecies += 1

        C = sparse.lil_matrix((self.c_nspecies, self.nspecies))
        for i in range(self.nspecies):
            C[i, i] = 1.0

        for s, d in self.composite.items():
            for component, factor in d.items():
                C[self.c_dspecies[s], self.dspecies[component]] = factor

        self.C = C.tocsr()
        self.CT = C.T.tocsr()
        
        
    def initialize(self):
        """ Builds the inner structures needed for running the model. """
        self.initialize_composites()

        # R[i, j] gives the contribution to dn[i]/dt of a reaction j
        R = zeros((self.c_nspecies, self.nreactions))

        # For a reaction j, F[j] is an array of species indices i1, i2,...
        # such that the production of j is k * dens[i1] * dens[i2] ...
        self.F = []

        # P contains 'prefactors' obtained by multiplying the densities
        # of the fixed species. 
        self.P = ones(self.nreactions)
        
        for j, r in enumerate(self.reactions):
            for f, s in r.rhs:
                try:
                    i = self.c_dspecies[s]
                    R[i, j] += f
                except KeyError:
                    # The species is fixed: ignore
                    pass
                

            l = []
            for f, s in r.lhs:
                try:
                    i = self.c_dspecies[s]
                    l.extend([i] * f)
                    R[i, j] -= f
                except KeyError:
                    # The species is fixed: include it in the pre-factor
                    self.P[j] *= self.fixed[s]
    
            self.F.append(array(l))

        # Count multiplicities in F (thus in reactions)
        self.M = array([[list(self.F[k]).count(j)
                         for k in range(self.nreactions)]
                        for j in range(self.c_nspecies)], dtype='i')
        self.M = sparse.csr_matrix(self.M)

        
        def remove_once(l, item):
            """ Returns a new list where item is removed from l but only
            once.  """
            newl = []
            rem = 1
            for i in l:
                if i != item or rem == 0:
                    newl.append(i)
                else:
                    rem -= 1

            return newl
        

        self.Flen = array([len(l) for l in self.F])
        self.FF = zeros((self.nreactions, 16), order='F')

        for i in range(self.nreactions):
            self.FF[i, :self.Flen[i]] = self.F[i]
            
        self.F1 = [[remove_once(self.F[k], j) for k in range(self.nreactions)]
                   for j in range(self.c_nspecies)]
        
        self.R = sparse.csr_matrix(R)
        

    def rates(self, *args):
        """ Calculates the reaction rates, including the prefactors
        (i.e. fixed densities).  """
        
        return (c_[[r.rate(*args) for r in self.reactions]]
                * self.P[:, newaxis])

    
    def derivs(self, n, *args, **kwargs):
        """ Calculates the dn/dt for each species from the densitites
        n and the status:
        
        n:      Array with shape n[nspecies, m]
        args:   Arguments to calculate rates
        """

        rates = kwargs.get('rates', None)
        
        # First we calculate all reaction rates [rates ~ (m, nreactions)].
        if rates is None:
            rates = self.rates(*args)

        # We calculate only in as many point as the length of args[0]
        try:
            m = len(args[0])
        except TypeError:
            m = 1

        # Calculate the composites
        if self.C is not None:
            n = self.C.dot(n)

        # Now the density products
        nprod = array([prod(n[self.F[j], :m], axis=0)
                       for j in range(self.nreactions)])
        
        # Then we are set.
        f = self.R.dot(nprod * rates)

        if self.C is not None:
            f = self.CT.dot(f)

        return f

    
    def fjacobian(self, n, *args, **kwargs):
        """ Calculates the jacobian. """

        rates = kwargs.get('rates', None)

        # First we calculate all reaction rates [rates ~ (m, nreactions)].
        if rates is None:
            rates = self.rates(*args)
        
        if self.C is not None:
            n = self.C.dot(n)

        J = _chemise.jacobian(n.T, rates.T, self.FF, self.Flen,
                              self.R.data, self.R.indices, self.R.indptr,
                              self.M.data, self.M.indices, self.M.indptr,
                              self.nreactions, self.c_nspecies, len(rargs[0])).T
        
        
        if self.C is not None:
            # With sparse matrices we can only perform multiplications
            # of the form S . A, where A is rank-2 and we run through A's
            # first index.  Therefore we have to do a lot of index gymnastics
            # to compute C.T . J . C.
            
            l1 = J.shape[0]
            l = self.nspecies 
            m = J.shape[2]
            A = self.C.T.dot(J.reshape((l1, -1))).reshape((l, l1, m))
            AT = swapaxes(A, 0, 1)
            B = self.C.T.dot(AT.reshape((l1, -1))).reshape((l, l, m))
            J = swapaxes(B, 0, 1)

        return J

    
    def fderivs(self, n, *args, **kwargs):
        # Reshape n and the args to make sure that it is of the form
        # (nspecies, nspace):        
        rargs = [ravel(v) for v in args]
        rn = n.reshape((n.shape[0], -1))
        
        rates = kwargs.get('rates', None)

        if rates is None:
            rates = self.rates(*rargs) 
        
        if self.C is not None:
            rn = self.C.dot(rn)

        f =  _chemise.derivs(rn.T, rates.T, self.FF, self.Flen,
                             self.R.data, self.R.indices, self.R.indptr,
                             self.nreactions, self.c_nspecies, len(rargs[0])).T

        if self.C is not None:
            f = self.CT.dot(f)

        return f.reshape(n.shape)

        
    def zero_densities(self, m):
        return zeros((self.nspecies, m))

    
    def set_species(self, n, s, newval):
        n[self.dspecies[s]] = newval

    def add_to_species(self, n, s, addval):
        n[self.dspecies[s]] += addval
        
    def get_species(self, n, s):
        return n[self.dspecies[s]]

    
    def species_vector(self, elements):
        """ Builds a vector of dimension (nspecies,) where nonzero elements
        are specified in the elements dictionary. """
        v = zeros(self.nspecies)
        for s, val in elements.items():
            v[self.dspecies[s]] = val

        return v

    
    def print_species(self, n, title=''):
        if title:
            print(title)
            print('-' * len(title))
            
        for i, s in enumerate(self.species):
            print('%-6s' % s, n[i, :])

        print()
        
    def print_summary(self):
        print("SPECIES:\n %s" % ", ".join(self.species))
        print("REACTIONS:\n  %s" 
              % "\n  ".join(str(x) for x in self.reactions))  

        print()
        

    def latex(self):
        def maybe_cite(r):
            try:
                return r"\cite{%s}" % r.attrs["ref"]
            except KeyError:
                return ""

        rows = []
        generics = set()
        
        for i, r in enumerate(self.reactions):
            try:
                generic = r.attrs['generic']
                latex = "\ce{%s}" % generic.replace("->", "&->")
            except KeyError:
                generic = None
                latex = r.latex()
                
            if generic is not None:
                if generic in generics:
                    continue
                else:
                    generics.add(generic)
                    
            row = ("\\rule{0pt}{1.75em}\\textbf{%d} & %s & %s & %s"
                   % (i + 1, latex, r.rate.latex(),
                      maybe_cite(r)))
            rows.append(row)

        return "\\\\\n".join(rows)


    #
    # QtPlaskin output
    #
    def qtp_init_single(self, path, conditions_list):
        if not os.path.exists(path):
            os.makedirs(path)

        with open(os.path.join(path, 'qt_species_list.txt'), 'w') as fsl:
            for i, s in enumerate(self.species):
                fsl.write("%d %s\n" % (i, str(s)))

        with open(os.path.join(path, 'qt_reactions_list.txt'), 'w') as frl:
            for i, r in enumerate(self.reactions):
                frl.write("%d %s\n" % (i, str(r)))

        with open(os.path.join(path, 'qt_conditions_list.txt'), 'w') as fcl:
            for i, c in enumerate(conditions_list):
                fcl.write("%d %s\n" % (i, c))

        if self.C is None:
            savetxt(os.path.join(path, 'qt_matrix.txt'), self.R.todense())
        else:
            R1 = self.CT.dot(self.R)
            savetxt(os.path.join(path, 'qt_matrix.txt'), R1.todense())
        
        # Here we just truncate these files in case they exist
        with open(os.path.join(path, 'qt_densities.txt'), 'w') as fd:
            pass
        
        with open(os.path.join(path, 'qt_rates.txt'), 'w') as fr:
            pass
        
        with open(os.path.join(path, 'qt_conditions.txt'), 'w') as fc:
            pass

        
    def qtp_init_many(self, path_list, conditions_list):
        for i, path in enumerate(path_list):
            self.qtp_init_single(path, conditions_list)

    
    def qtp_append_single(self, t, path, n, *args, **kwargs):
        rates = kwargs.get('rates', None)

        if self.C is not None:
            n1 = self.C.dot(n)
        else:
            n1 = n

        if rates is None:
            nprod = array([prod(n1[self.F[j]], axis=0)
                           if self.F[j] != [] else 1
                           for j in range(self.nreactions)])
            rates = nprod * squeeze(self.rates(*args))

            
        with open(os.path.join(path, 'qt_densities.txt'), 'a') as fd:
            fd.write(" ".join('%.18e' % x
                              for x in r_[t, squeeze(n) / co.centi**-3])
                     + "\n")

        with open(os.path.join(path, 'qt_rates.txt'), 'a') as fr:
            fr.write(" ".join('%.18e' % x
                              for x in r_[t, squeeze(rates) / co.centi**-3])
                     + "\n")

        with open(os.path.join(path, 'qt_conditions.txt'), 'a') as fc:
            fc.write(" ".join('%.18e' % x
                              for x in r_[t, squeeze(array(args))]) + "\n")


    def qtp_append_multi(self, t, path_list, n, *args, **kwargs):
        rates = kwargs.get('rates', None)

        if self.C is not None:
            n1 = self.C.dot(n)
        else:
            n1 = n
        
        if rates is None:
            nprod = array([prod(n1[self.F[j], :], axis=0)
                           if len(self.F[j]) > 0 else 1
                           for j in range(self.nreactions)])
            rates = nprod * self.rates(*args) 

        def pick_if_array(a, i):
            try:
                return a[i]
            except TypeError:
                return a
            
        for i, path in enumerate(path_list):
            sargs = [pick_if_array(v, i) for v in args]
            self.qtp_append_single(t, path, n[:, i], *sargs,
                                   rates=rates[:, i])

    
            

class Rate(object):
    def __call__(self, *args):
        raise NotImplementedError("You must specify the reaction rate")


    def latex(self):
        return "\\textbf{Unknown!}"

    
class Constant(Rate):
    def __init__(self, k):
        self.k = k

    def __call__(self, *args):
        return full_like(args[0], self.k)

    def latex(self, factor=1):
        return r"\num{%g}" % (self.k / factor)
    
    
class InterpolateN(Rate):
    """ Rate class to interpolate on the nth argument.  """
    def __init__(self, n, fname, cols=(0, 1), prefactor=1.0,
                 zero_value=None, extend=False):
        d = loadtxt(fname, unpack=True)
        x, k = d[cols[0], :], d[cols[1], :]

        k *= prefactor
        
        if zero_value is not None:
            x = r_[0.0, x]
            k = r_[zero_value, k]

        if extend:
            x = r_[x, 10 * x[-1]]
            k = r_[k, k[-1]]
            
        #self.s = UniformInterpolator(x, prefactor * k)
        self.s = interp1d(x, k)
        self.n = n
        
    def latex(self, factor=1):
        return "$f(x_%d)$" % self.n

    def __call__(self, *args):
        return self.s(args[self.n])


class Interpolate0(InterpolateN):
    def __init__(self, *args, **kwargs):
        super(Interpolate0, self).__init__(0, *args, **kwargs)
        
        
class LogInterpolateN(Rate):
    def __init__(self, n, fname, cols=(0, 1), prefactor=1.0,
                 extend=False):
        d = loadtxt(fname, unpack=True)
        x, k = d[cols[0], :], d[cols[1], :]

        flt = k > 0
        
        if extend:
            self.x = r_[self.x, 10 * self.x[-1]]
            self.k = r_[self.k, self.k[-1]]

        self.s = interp1d(x[flt], log(prefactor * k[flt]))
        self.n = n
        
    def latex(self, factor=1):
        return "$f(x_0)$"

    def __call__(self, *args):
        return exp(self.s(args[self.n]))


class LogInterpolate0(LogInterpolateN):
    def __init__(self, *args, **kwargs):
        super(LogInterpolate0, self).__init__(0, *args, **kwargs)


class LogLogInterpolateN(Rate):
    def __init__(self, n, fname, cols=(0, 1), prefactor=1.0, extend=False):
        d = loadtxt(fname, unpack=True)
        self.x, self.k = d[cols[0], :], d[cols[1], :]

        if extend:
            self.x = r_[self.x, 10 * self.x[-1]]
            self.k = r_[self.k, self.k[-1]]

        flt = self.k > 0
        
        self.s = interp1d(log(self.x[flt]), log(prefactor * self.k[flt]),
                          bounds_error=False,
                          fill_value=log(prefactor * self.k[0]))
        
        self.n = n
        
    def latex(self, factor=1):
        return "$f(x_0)$"

    def __call__(self, *args):
        return exp(self.s(log(args[self.n])))


class LogLogInterpolate0(LogLogInterpolateN):
    def __init__(self, *args, **kwargs):
        super(LogLogInterpolate0, self).__init__(0, *args, **kwargs)

        
class UniformInterpolator(object):
    def __init__(self, x, y, n=None):
        if n is None:
            n = len(x)

        self.nonuniform = interp1d(x, y)
        self.x = linspace(x[0], x[-1], n)
        self.y = self.nonuniform(self.x)
        self.dx = self.x[1] - self.x[0]

        
    def __call__(self, newx):
        """ Interpolates for the values contained in newx. """
        return _chemise.uinterpol1d(self.x[0], self.dx, self.y, newx)

        
RE_ARROW = re.compile(r"\s*[-=]+>\s*")
RE_PLUS = re.compile(r"\s+\+\s+")
RE_MULTIPLICITY = re.compile(r"((\d+)\s*\*\s*)?([\w\(\).^+*-]+)")

def parse(signature):
    lhs, rhs = (RE_PLUS.split(x) for x in RE_ARROW.split(signature))
    def multiplicity(code):
        m = RE_MULTIPLICITY.match(code)
        n, s = m.group(2), m.group(3)

        n = int(n) if n is not None else 1

        return (n, s)

    lhs, rhs = [[multiplicity(item) for item in x if item] for x in (lhs, rhs)]

    return lhs, rhs

